use std::any::TypeId;

#[derive(Clone, Copy, Debug)]
pub struct ConstTypeId(u64, u64);

impl ConstTypeId {
    pub const fn of<T: 'static>() -> Self {
        let type_id = TypeId::of::<T>();
        // SAFETY: While there is no guarantee that the u64's will be in the same order,
        // all we need in this case is the data and it is valid on both ends so this is sound.
        unsafe { std::mem::transmute(type_id) }
    }

    pub const fn equal(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}
