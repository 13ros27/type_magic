use crate::const_list::{ArrayLength, ConstList};
use crate::const_type_id::ConstTypeId;
use const_panic::concat_panic;
use std::marker::PhantomData;
use std::ops::Add;
use typenum::{Sum, Unsigned, U1};

const fn check_access_conflicts<D: QueryData>() {
    let mut i = 0;
    while i < D::LENGTH::USIZE {
        let mut j = 0;
        while j < i {
            let ith = D::DATA.get(i);
            let jth = D::DATA.get(j);
            if ith.0.equal(&jth.0) {
                if ith.1 {
                    concat_panic!("&mut ", display: D::NAMES.get(i), " conflicts with a previous access in this query. Mutable component access must be unique.")
                } else if jth.1 {
                    concat_panic!("&", display: D::NAMES.get(i)," conflicts with a previous access in this query. Shared access cannot coincide with exclusive access.");
                }
            }
            j += 1;
        }
        i += 1;
    }
}

struct Query<D: QueryData> {
    _marker: PhantomData<D>,
}

impl<D: QueryData> Default for Query<D> {
    fn default() -> Self {
        struct Check<D: QueryData>(D);
        impl<D: QueryData> Check<D> {
            const ACCESS_CONFLICT: () = check_access_conflicts::<D>();
        }
        let _: () = Check::<D>::ACCESS_CONFLICT;
        println!("{:?}, {:?}", D::DATA.as_slice(), D::DATA.get(3));
        Self {
            _marker: PhantomData,
        }
    }
}

trait Component: 'static {
    const NAME: &'static str;
}

type QueryDataList<N> = ConstList<(ConstTypeId, bool), N>;

trait QueryData {
    type LENGTH: ArrayLength;
    const DATA: QueryDataList<Self::LENGTH>;
    const NAMES: ConstList<&'static str, Self::LENGTH>;
}

impl<C: Component> QueryData for &C {
    type LENGTH = U1;
    const DATA: QueryDataList<U1> = QueryDataList::new((ConstTypeId::of::<C>(), false));
    const NAMES: ConstList<&'static str, U1> = ConstList::new(C::NAME);
}

impl<C: Component> QueryData for &mut C {
    type LENGTH = U1;
    const DATA: QueryDataList<U1> = QueryDataList::new((ConstTypeId::of::<C>(), true));
    const NAMES: ConstList<&'static str, U1> = ConstList::new(C::NAME);
}

impl<D: QueryData> QueryData for (D,) {
    type LENGTH = D::LENGTH;
    const DATA: QueryDataList<D::LENGTH> = D::DATA;
    const NAMES: ConstList<&'static str, D::LENGTH> = D::NAMES;
}

impl<A, B> QueryData for (A, B)
where
    A: QueryData,
    B: QueryData,
    A::LENGTH: Add<B::LENGTH>,
    Sum<A::LENGTH, B::LENGTH>: ArrayLength,
{
    type LENGTH = Sum<A::LENGTH, B::LENGTH>;
    const DATA: QueryDataList<Self::LENGTH> = A::DATA.join(B::DATA);
    const NAMES: ConstList<&'static str, Self::LENGTH> = A::NAMES.join(B::NAMES);
}

impl<A, B, C> QueryData for (A, B, C)
where
    (A, B): QueryData,
    C: QueryData,
    <(A, B) as QueryData>::LENGTH: Add<C::LENGTH>,
    Sum<<(A, B) as QueryData>::LENGTH, C::LENGTH>: ArrayLength,
{
    type LENGTH = Sum<<(A, B) as QueryData>::LENGTH, C::LENGTH>;
    const DATA: QueryDataList<Self::LENGTH> = <(A, B)>::DATA.join(C::DATA);
    const NAMES: ConstList<&'static str, Self::LENGTH> = <(A, B)>::NAMES.join(C::NAMES);
}

impl<A, B, C, D> QueryData for (A, B, C, D)
where
    (A, B): QueryData,
    (C, D): QueryData,
    <(A, B) as QueryData>::LENGTH: Add<<(C, D) as QueryData>::LENGTH>,
    Sum<<(A, B) as QueryData>::LENGTH, <(C, D) as QueryData>::LENGTH>: ArrayLength,
{
    type LENGTH = Sum<<(A, B) as QueryData>::LENGTH, <(C, D) as QueryData>::LENGTH>;
    const DATA: QueryDataList<Self::LENGTH> = <(A, B)>::DATA.join(<(C, D)>::DATA);
    const NAMES: ConstList<&'static str, Self::LENGTH> = <(A, B)>::NAMES.join(<(C, D)>::NAMES);
}
