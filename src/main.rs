#![feature(const_type_id, portable_simd)]
#![recursion_limit = "256"]

mod const_list;
mod const_type_id;
mod query;
mod refinement;

fn main() {}
