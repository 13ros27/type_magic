use super::Refined;
use core::ops::{Index, IndexMut};
use typenum::{Const, GrEq, IsGreaterOrEqual, NonZero, ToUInt, Unsigned};

impl<I, T, S, E, const N: usize> Index<Refined<I, S, E>> for [T; N]
where
    I: Into<usize>,
    S: Unsigned,
    E: Unsigned,
    Const<N>: ToUInt,
    <Const<N> as ToUInt>::Output: IsGreaterOrEqual<E>,
    GrEq<<Const<N> as ToUInt>::Output, E>: NonZero,
{
    type Output = T;
    fn index(&self, index: Refined<I, S, E>) -> &T {
        // SAFETY: The bounds are checked by the trait bounds (specifically GrEq)
        unsafe { self.get_unchecked(index.to_int().into()) }
    }
}

impl<I, T, S, E, const N: usize> IndexMut<Refined<I, S, E>> for [T; N]
where
    I: Into<usize>,
    S: Unsigned,
    E: Unsigned,
    Const<N>: ToUInt,
    <Const<N> as ToUInt>::Output: IsGreaterOrEqual<E>,
    GrEq<<Const<N> as ToUInt>::Output, E>: NonZero,
{
    fn index_mut(&mut self, index: Refined<I, S, E>) -> &mut T {
        // SAFETY: The bounds are checked by the trait bounds (specifically GrEq)
        unsafe { self.get_unchecked_mut(index.to_int().into()) }
    }
}
