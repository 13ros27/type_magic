use super::{unsigned::UnsignedType, Refined};
use core::ops::{Add, Sub};
use core::simd::{LaneCount, Simd, SimdElement, SupportedLaneCount};
use typenum::{Add1, ToInt, ToUInt, Unsigned, B1};

// Refined<T, S, E> - Const<N> = Refined<T, S - N, E - N>
impl<T, S, E, N> Sub<N> for Refined<T, S, E>
where
    T: UnsignedType + Sub<Output = T>,
    S: Unsigned + Sub<N::Output>,
    E: Unsigned + Sub<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
{
    type Output = Refined<T, <S as Sub<N::Output>>::Output, <E as Sub<N::Output>>::Output>;
    fn sub(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we subtracted N from both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() - N::Output::to_int()) }
    }
}

// Refined<T, S1, E1> - Refined<T, S2, E2> = Refined<T, S1 - S2 + 1, E1 - E2>
impl<T, S1, E1, S2, E2> Sub<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Sub<Output = T>,
    S1: Unsigned + Sub<E2>,
    E1: Unsigned + Sub<S2>,
    S2: Unsigned,
    E2: Unsigned,
    S1::Output: Add<B1>,
    E1::Output: Unsigned,
    <S1::Output as Add<B1>>::Output: Unsigned,
{
    type Output = Refined<T, Add1<<S1 as Sub<E2>>::Output>, <E1 as Sub<S2>>::Output>;
    fn sub(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: We have widened the bounds so that it is within (S1 - (E2 - 1))..(E1 - S2)
        unsafe { Refined::new_unchecked(self.to_int() - rhs.to_int()) }
    }
}

// Refined<Simd<T, LANES>, S, E> - Const<N> = Refined<Simd<T, LANES>, S - N, E - N>
impl<T, S, E, N, const LANES: usize> Sub<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned + Sub<N::Output>,
    E: Unsigned + Sub<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
    Simd<T, LANES>: Sub<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output =
        Refined<Simd<T, LANES>, <S as Sub<N::Output>>::Output, <E as Sub<N::Output>>::Output>;
    fn sub(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we subtracted N from both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() - Simd::splat(N::Output::to_int())) }
    }
}
