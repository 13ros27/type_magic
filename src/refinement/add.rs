use super::{unsigned::UnsignedType, Refined};
use core::ops::{Add, Sub};
use core::simd::{LaneCount, Simd, SimdElement, SupportedLaneCount};
use typenum::{Sub1, ToInt, ToUInt, Unsigned, B1};

// Refined<T, S, E> + Const<N> = Refined<T, S + N, E + N>
impl<T, S, E, N> Add<N> for Refined<T, S, E>
where
    T: UnsignedType + Add<Output = T>,
    S: Unsigned + Add<N::Output>,
    E: Unsigned + Add<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
{
    type Output = Refined<T, <S as Add<N::Output>>::Output, <E as Add<N::Output>>::Output>;
    fn add(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we added N to both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() + N::Output::to_int()) }
    }
}

// Refined<T, S1, E1> + Refined<T, S2, E2> = Refined<T, S1 + S2, E1 + E2 - 1>
impl<T, S1, E1, S2, E2> Add<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Add<Output = T>,
    S1: Unsigned + Add<S2>,
    E1: Unsigned + Add<E2>,
    S2: Unsigned,
    E2: Unsigned,
    S1::Output: Unsigned,
    E1::Output: Sub<B1>,
    <E1::Output as Sub<B1>>::Output: Unsigned,
{
    type Output = Refined<T, <S1 as Add<S2>>::Output, Sub1<<E1 as Add<E2>>::Output>>;
    fn add(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: We have widened the bounds so that S1 + S2 and (E1 - 1) + (E2 - 1) are within bounds so this must be
        unsafe { Refined::new_unchecked(self.to_int() + rhs.to_int()) }
    }
}

// Refined<Simd<T, LANES>, S, E> + Const<N> = Refined<Simd<T, LANES>, S + N, E + N>
impl<T, S, E, N, const LANES: usize> Add<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned + Add<N::Output>,
    E: Unsigned + Add<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
    Simd<T, LANES>: Add<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output =
        Refined<Simd<T, LANES>, <S as Add<N::Output>>::Output, <E as Add<N::Output>>::Output>;
    fn add(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we added N to both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() + Simd::splat(N::Output::to_int())) }
    }
}
