use super::{unsigned::UnsignedType, Refined};
use core::ops::Rem;
use core::simd::{LaneCount, Simd, SimdElement, SupportedLaneCount};
use typenum::{ToInt, ToUInt, Unsigned, U0};

// Refined<T, S, E> % Const<N> = Refined<T, 0, N>
impl<T, S, E, N> Rem<N> for Refined<T, S, E>
where
    T: UnsignedType + Rem<Output = T>,
    S: Unsigned,
    E: Unsigned,
    N: ToUInt,
    N::Output: Unsigned + ToInt<T>,
{
    type Output = Refined<T, U0, N::Output>;
    fn rem(self, _: N) -> Self::Output {
        // SAFETY: Modular arithmetic cannot be larger than its modulus (N)
        unsafe { Refined::new_unchecked(self.to_int() % N::Output::to_int()) }
    }
}

// Refined<Simd<T, LANES>, S, E> % Const<N> = Refined<Simd<T, LANES>, 0, N>
impl<T, S, E, N, const LANES: usize> Rem<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned,
    E: Unsigned,
    N: ToUInt,
    N::Output: Unsigned + ToInt<T>,
    Simd<T, LANES>: Rem<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output = Refined<Simd<T, LANES>, U0, N::Output>;
    fn rem(self, _: N) -> Self::Output {
        // SAFETY: Modular arithmetic cannot be larger than its modulus (N)
        unsafe { Refined::new_unchecked(self.to_int() % Simd::splat(N::Output::to_int())) }
    }
}
