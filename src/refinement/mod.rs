use core::fmt::{self, Debug, Formatter};
use core::marker::PhantomData;
use core::mem::{size_of, ManuallyDrop};
use core::ptr;
use core::simd::{
    cmp::{SimdPartialEq, SimdPartialOrd},
    num::SimdUint,
    LaneCount, Mask, Simd, SimdCast, SimdElement, SupportedLaneCount,
};
use typenum::{
    Const, GrEq, IsGreaterOrEqual, IsLessOrEqual, LeEq, NonZero, ToInt, ToUInt, Unsigned,
};

pub use range::ConstRange;
use unsigned::UnsignedType;

mod add;
mod index;
mod mul;
mod range;
mod rem;
mod sub;
mod unsigned;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Refined<T, S: Unsigned, E: Unsigned>(T, PhantomData<(S, E)>); // Range-exclusive (S..E)

impl<T, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    /// # Safety
    /// `num` must be in S..E
    pub const unsafe fn new_unchecked(num: T) -> Self {
        Self(num, PhantomData)
    }

    #[inline]
    #[allow(clippy::wrong_self_convention)]
    pub fn to_int(self) -> T {
        self.0
    }

    #[inline]
    pub fn widen<S2, E2>(self) -> Refined<T, S2, E2>
    where
        S2: Unsigned + IsLessOrEqual<S>,
        E2: Unsigned + IsGreaterOrEqual<E>,
        LeEq<S2, S>: NonZero,
        GrEq<E2, E>: NonZero, // TODO: Could use special traits and `diagnostic::on_unimplemented` to improve errors
    {
        // SAFETY: The bounds must be larger than or equal
        unsafe { Refined::new_unchecked(self.to_int()) }
    }

    #[inline]
    /// # Safety
    /// `self` must be in S2..E2
    pub unsafe fn shrink<S2: Unsigned, E2: Unsigned>(self) -> Refined<T, S2, E2> {
        // SAFETY: Enforced by the safety invariants of `shrink`
        unsafe { Refined::new_unchecked(self.to_int()) }
    }
}

impl<T, S, E> Refined<T, S, E>
where
    T: PartialOrd,
    S: Unsigned + ToInt<T>,
    E: Unsigned + ToInt<T>,
{
    #[inline]
    pub fn new(num: T) -> Option<Self> {
        if num >= S::to_int() && num < E::to_int() {
            // SAFETY: We checked that `num` is within S..E
            unsafe { Some(Refined::new_unchecked(num)) }
        } else {
            None
        }
    }
}

impl<T, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub const fn constant<const N: usize>() -> Self
    where
        S: IsLessOrEqual<<Const<N> as ToUInt>::Output>,
        Const<N>: ToUInt,
        <Const<N> as ToUInt>::Output: Unsigned + ToInt<T> + IsLessOrEqual<E>,
        LeEq<S, <Const<N> as ToUInt>::Output>: NonZero,
        LeEq<<Const<N> as ToUInt>::Output, E>: NonZero,
    {
        // SAFETY: Checked in the type bounds
        unsafe { Refined::new_unchecked(<Const<N> as ToUInt>::Output::INT) }
    }
}

impl<T: UnsignedType, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn cast<T2: UnsignedType>(self) -> Refined<T2, S, E>
    where
        T2::SIZE: IsGreaterOrEqual<E>,
        GrEq<T2::SIZE, E>: NonZero,
        T: TryInto<T2>,
    {
        // SAFETY: We check it can't overflow with the `IsGreater` type bound
        let cast = unsafe { self.to_int().try_into().unwrap_unchecked() };
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(cast) }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned, const LANES: usize> Refined<Simd<T, LANES>, S, E>
where
    LaneCount<LANES>: SupportedLaneCount,
{
    #[inline]
    pub fn cast<T2: SimdCast>(self) -> Refined<Simd<T2, LANES>, S, E>
    where
        Simd<T, LANES>: SimdUint<Cast<T2> = Simd<T2, LANES>>,
    {
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(self.to_int().cast::<T2>()) }
    }
}

// TODO: Combine these via passing to another trait so you can be generic over Simd
impl<T: Copy + UnsignedType + PartialOrd, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn split<M: Unsigned + ToInt<T>, R>(
        self,
        left: impl Fn(Refined<T, S, M>) -> R,
        right: impl Fn(Refined<T, M, E>) -> R,
    ) -> R {
        if self.to_int() < M::to_int() {
            // SAFETY: We just checked if it is in this section of S..E
            left(unsafe { self.shrink() })
        } else {
            // SAFETY: We just checked if it is in this section of S..E
            right(unsafe { self.shrink() })
        }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned, const LANES: usize> Refined<Simd<T, LANES>, S, E>
where
    LaneCount<LANES>: SupportedLaneCount,
    Simd<T, LANES>: Copy + SimdPartialOrd + SimdPartialEq<Mask = Mask<T::Mask, LANES>>,
{
    #[inline]
    pub fn split<M: Unsigned + ToInt<T>, S2: Unsigned, E2: Unsigned>(
        self,
        left: impl Fn(Refined<Simd<T, LANES>, S, M>) -> Refined<Simd<T, LANES>, S2, E2>,
        right: impl Fn(Refined<Simd<T, LANES>, M, E>) -> Refined<Simd<T, LANES>, S2, E2>,
    ) -> Refined<Simd<T, LANES>, S2, E2> {
        let mask = self.to_int().simd_lt(Simd::splat(M::to_int()));
        let unrefined = mask.select(
            // SAFETY: We just checked if it is in this section of S..E
            left(unsafe { self.shrink() }).to_int(),
            // SAFETY: We just checked if it is in this section of S..E
            right(unsafe { self.shrink() }).to_int(),
        );
        // SAFETY: `left` and `right` return this refined so it must have been generated correctly
        unsafe { Refined::new_unchecked(unrefined) }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn simd_splat<const LANES: usize>(self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount,
    {
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(Simd::splat(self.to_int())) }
    }
}

impl<T: Debug, S: Unsigned + ToInt<T>, E: Unsigned + ToInt<T>> Debug for Refined<T, S, E> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?} is {:?}..{:?}", self.0, S::to_int(), E::to_int())
    }
}

pub trait RefinedSimdExt<T: SimdElement, S: Unsigned, E: Unsigned> {
    fn truncate_as_simd<const LANES: usize>(&self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount;

    #[allow(clippy::type_complexity)]
    fn as_ref_simd<const LANES: usize>(
        &self,
    ) -> (
        &[Refined<T, S, E>],
        &[Refined<Simd<T, LANES>, S, E>],
        &[Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsRef<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount;

    #[allow(clippy::type_complexity)]
    fn as_ref_simd_mut<const LANES: usize>(
        &mut self,
    ) -> (
        &mut [Refined<T, S, E>],
        &mut [Refined<Simd<T, LANES>, S, E>],
        &mut [Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsMut<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount;
}

impl<T: SimdElement, S: Unsigned, E: Unsigned> RefinedSimdExt<T, S, E> for [Refined<T, S, E>] {
    #[inline]
    fn truncate_as_simd<const LANES: usize>(&self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_ref(self) as *const [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let simd = unsafe { Simd::from_slice(&*unrefined) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        unsafe { cursed_transmute(simd) }
    }

    #[allow(clippy::type_complexity)]
    #[inline]
    fn as_ref_simd<const LANES: usize>(
        &self,
    ) -> (
        &[Refined<T, S, E>],
        &[Refined<Simd<T, LANES>, S, E>],
        &[Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsRef<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_ref(self) as *const [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let (prefix, middle, suffix) = unsafe { (*unrefined).as_simd() };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let prefix = unsafe { &*(ptr::from_ref(prefix) as *const [Refined<T, S, E>]) };
        // SAFETY: `Simd` doesn't change the values so they are still in S..E and `Refined` is repr(transparent)
        let middle = unsafe { &*(ptr::from_ref(middle) as *const [Refined<Simd<T, LANES>, S, E>]) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let suffix = unsafe { &*(ptr::from_ref(suffix) as *const [Refined<T, S, E>]) };
        (prefix, middle, suffix)
    }

    #[allow(clippy::type_complexity)]
    #[inline]
    fn as_ref_simd_mut<const LANES: usize>(
        &mut self,
    ) -> (
        &mut [Refined<T, S, E>],
        &mut [Refined<Simd<T, LANES>, S, E>],
        &mut [Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsMut<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_mut(self) as *mut [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let (prefix, middle, suffix) = unsafe { (*unrefined).as_simd_mut() };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let prefix = unsafe { &mut *(ptr::from_ref(prefix) as *mut [Refined<T, S, E>]) };
        // SAFETY: `Simd` doesn't change the values so they are still in S..E and `Refined` is repr(transparent)
        let middle =
            unsafe { &mut *(ptr::from_ref(middle) as *mut [Refined<Simd<T, LANES>, S, E>]) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let suffix = unsafe { &mut *(ptr::from_ref(suffix) as *mut [Refined<T, S, E>]) };
        (prefix, middle, suffix)
    }
}

/// Allows types that can't be size-checked pre-monomorphisation and allows you to downcast to a smaller size type.
/// # Safety
/// Same as `mem::transmute`
pub(crate) const unsafe fn cursed_transmute<Src, Dst>(src: Src) -> Dst {
    struct Check<Src, Dst>(Src, Dst);
    impl<Src, Dst> Check<Src, Dst> {
        const SIZE_MISMATCH: () = assert!(
            size_of::<Src>() <= size_of::<Dst>(),
            "Size mismatch in cursed_transmute"
        );
    }
    let _: () = Check::<Src, Dst>::SIZE_MISMATCH;

    #[allow(clippy::items_after_statements)]
    #[repr(C)]
    union Transmute<From, To> {
        from: ManuallyDrop<From>,
        to: ManuallyDrop<To>,
    }

    // SAFETY: `Check` ensures the sizes are valid, everything else is a safety invariant
    let manual_dst: ManuallyDrop<Dst> = unsafe {
        Transmute {
            from: ManuallyDrop::new(src),
        }
        .to
    };
    ManuallyDrop::into_inner(manual_dst)
}
