use typenum::{Pow, Unsigned, U16, U2, U256, U4, U8};

pub trait UnsignedType: sealed::Sealed + Copy {
    type SIZE: Unsigned;
    const ONE: Self;
}

impl UnsignedType for u8 {
    type SIZE = U256;
    const ONE: Self = 1;
}

impl UnsignedType for u16 {
    type SIZE = <U256 as Pow<U2>>::Output;
    const ONE: Self = 1;
}

impl UnsignedType for u32 {
    type SIZE = <U256 as Pow<U4>>::Output;
    const ONE: Self = 1;
}

impl UnsignedType for u64 {
    type SIZE = <U256 as Pow<U8>>::Output;
    const ONE: Self = 1;
}

impl UnsignedType for u128 {
    type SIZE = <U256 as Pow<U16>>::Output;
    const ONE: Self = 1;
}

#[cfg(target_pointer_width = "16")]
impl UnsignedType for usize {
    type SIZE = <U256 as Pow<U2>>::Output;
    const ONE: Self = 1;
}

#[cfg(target_pointer_width = "32")]
impl UnsignedType for usize {
    type SIZE = <U256 as Pow<U4>>::Output;
    const ONE: Self = 1;
}

#[cfg(target_pointer_width = "64")]
impl UnsignedType for usize {
    type SIZE = <U256 as Pow<U8>>::Output;
    const ONE: Self = 1;
}

mod sealed {
    pub trait Sealed {}
    impl Sealed for u8 {}
    impl Sealed for u16 {}
    impl Sealed for u32 {}
    impl Sealed for u64 {}
    impl Sealed for u128 {}
    impl Sealed for usize {}
}
