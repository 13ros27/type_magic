use super::{unsigned::UnsignedType, Refined};
use core::iter::FusedIterator;
use core::marker::PhantomData;
use core::ops::{AddAssign, Range};
use typenum::{ToInt, Unsigned};

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ConstRange<T: UnsignedType, S: Unsigned, E: Unsigned>(Range<T>, PhantomData<(S, E)>);

impl<T, S, E> Default for ConstRange<T, S, E>
where
    T: UnsignedType,
    S: Unsigned + ToInt<T>,
    E: Unsigned + ToInt<T>,
{
    fn default() -> Self {
        ConstRange(S::to_int()..E::to_int(), PhantomData)
    }
}

impl<T, S, E> Iterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
    type Item = Refined<T, S, E>;
    fn next(&mut self) -> Option<Refined<T, S, E>> {
        self.0.next().map(|u| {
            // SAFETY: ConstRange is only constructed via Default which makes `self.0` always in S..E
            unsafe { Refined::new_unchecked(u) }
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl<T, S, E> ExactSizeIterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
}

impl<T, S, E> FusedIterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
}
