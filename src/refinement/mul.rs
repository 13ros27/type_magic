use super::{unsigned::UnsignedType, Refined};
use core::ops::{Add, Mul, Sub};
use typenum::{Add1, Sub1, ToInt, ToUInt, Unsigned, B1};

// Refined<T, S, E> * Const<N> = Refined<T, S * N, (E - 1) * N + 1>
impl<T, S, E, N> Mul<N> for Refined<T, S, E>
where
    T: UnsignedType + Mul<Output = T>,
    S: Unsigned + Mul<N::Output>,
    E: Unsigned + Sub<B1>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Mul<N::Output>,
    N::Output: ToInt<T>,
    <E::Output as Mul<N::Output>>::Output: Add<B1>,
    <<E::Output as Mul<N::Output>>::Output as Add<B1>>::Output: Unsigned,
{
    type Output =
        Refined<T, <S as Mul<N::Output>>::Output, Add1<<Sub1<E> as Mul<N::Output>>::Output>>;
    fn mul(self, _: N) -> Self::Output {
        // SAFETY: The bounds have been widened to (S * N)..((E - 1) * N + 1)
        unsafe { Refined::new_unchecked(self.to_int() * N::Output::to_int()) }
    }
}

// Refined<T, S1, E1> * Refined<T, S2, E2> = Refined<T, S1 * S2, (E1 - 1) * (E2 - 1) + 1>
impl<T, S1, E1, S2, E2> Mul<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Mul<Output = T>,
    S1: Unsigned + Mul<S2>,
    S2: Unsigned,
    E1: Unsigned + Sub<B1>,
    E2: Unsigned + Sub<B1>,
    S1::Output: Unsigned,
    E1::Output: Mul<E2::Output>,
    <E1::Output as Mul<E2::Output>>::Output: Add<B1>,
    <<E1::Output as Mul<E2::Output>>::Output as Add<B1>>::Output: Unsigned,
{
    type Output = Refined<T, <S1 as Mul<S2>>::Output, Add1<<Sub1<E1> as Mul<Sub1<E2>>>::Output>>;
    fn mul(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: The bounds have been widened to (S1 * S2)..((E1 - 1) * (E2 - 1) + 1)
        unsafe { Refined::new_unchecked(self.to_int() * rhs.to_int()) }
    }
}
