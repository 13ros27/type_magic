use generic_array::{GenericArray, IntoArrayLength};
use std::marker::PhantomData;
use std::mem::{size_of, ManuallyDrop};
use std::ops::Add;
use std::slice::from_raw_parts;
use typenum::{Const, Sum, UInt, UTerm, Unsigned, B0, B1, U0, U1};

#[repr(transparent)]
pub struct ConstList<T: 'static, N: ArrayLength>(ManuallyDrop<GenericArray<T, N>>);

impl<T> ConstList<T, U1> {
    pub const fn new(item: T) -> Self {
        Self::from_array([item])
    }
}

impl<T, N: ArrayLength> ConstList<T, N> {
    pub const fn from_array<const M: usize>(value: [T; M]) -> Self
    where
        Const<M>: IntoArrayLength<ArrayLength = N>, // Const<M>: ToUInt,
                                                    // U<M>: IsEqual<N>,
    {
        ConstList(ManuallyDrop::new(GenericArray::from_array(value)))
    }

    pub const fn join<M: ArrayLength>(self, other: ConstList<T, M>) -> ConstList<T, Sum<N, M>>
    where
        N: Add<M>,
        Sum<N, M>: ArrayLength,
    {
        #[repr(C)]
        struct Pair<A, B> {
            left: A,
            right: B,
        }

        let combined = Pair {
            left: self,
            right: other,
        };

        // SAFETY: N + M is equivalent to Sum<N, M> so they will have the same size
        ConstList(unsafe { cursed_transmute(combined) })
    }

    pub const fn get(self, index: usize) -> T {
        if index >= N::USIZE {
            panic!("Tried to index past the end of a ConstList");
        }

        if N::USIZE % 2 == 0 {
            let even: LocalGenericArrayImplEven<T, ConstList<T, N::HalfSize>> =
                unsafe { cursed_transmute(self) };
            if index < N::HalfSize::USIZE {
                even.parent1.get(index)
            } else {
                even.parent2.get(index - N::HalfSize::USIZE)
            }
        } else {
            let odd: LocalGenericArrayImplOdd<ManuallyDrop<T>, ConstList<T, N::HalfSize>> =
                unsafe { cursed_transmute(self) };
            if index == N::USIZE - 1 {
                ManuallyDrop::into_inner(odd.data)
            } else if index < N::HalfSize::USIZE {
                odd.parent1.get(index)
            } else {
                odd.parent2.get(index - N::HalfSize::USIZE)
            }
        }
    }

    pub const fn get_const<M: ArrayLength>(self) -> T {
        if M::USIZE >= N::USIZE {
            panic!("Tried to index past the end of a ConstList");
        }

        #[repr(C)]
        struct PaddedItem<T: 'static, M: ArrayLength> {
            _padding: ManuallyDrop<ConstList<T, M>>,
            item: ManuallyDrop<T>,
        }
        let padded: PaddedItem<T, M> = unsafe { cursed_transmute(self) };
        ManuallyDrop::into_inner(padded.item)
    }

    // NOTE: Can't currently be called in const without feature(const_refs_to_cell) because ConstList is recursive (may need to be unsafe due to use-after-free potential?)
    pub const fn as_slice(&self) -> &[T] {
        // SAFETY: ConstList is a transparent wrapper around GenericArray which can be safely cast to slice
        unsafe { from_raw_parts(self as *const ConstList<T, N> as *const T, N::USIZE) }
    }
}

/// Allows types that can't be size-checked pre-monomorphisation and allows you to downcast to a smaller size type.
/// SAFETY: Same as `mem::transmute`
const unsafe fn cursed_transmute<Src, Dst>(src: Src) -> Dst {
    struct Check<Src, Dst>(Src, Dst);
    impl<Src, Dst> Check<Src, Dst> {
        const SIZE_MISMATCH: () = assert!(
            size_of::<Src>() <= size_of::<Dst>(),
            "Size mismatch in cursed_transmute"
        );
    }
    let _: () = Check::<Src, Dst>::SIZE_MISMATCH;

    #[repr(C)]
    union Transmute<From, To> {
        from: ManuallyDrop<From>,
        to: ManuallyDrop<To>,
    }

    let manual_dst: ManuallyDrop<Dst> = unsafe {
        Transmute {
            from: ManuallyDrop::new(src),
        }
        .to
    };
    ManuallyDrop::into_inner(manual_dst)
}

// These just exist so that I can access their fields
#[repr(C)]
struct LocalGenericArrayImplEven<T, U> {
    parent1: U,
    parent2: U,
    _marker: PhantomData<T>,
}

#[repr(C)]
struct LocalGenericArrayImplOdd<T, U> {
    parent1: U,
    parent2: U,
    data: T,
}

pub trait ArrayLength: generic_array::ArrayLength {
    type HalfSize: ArrayLength;
}

impl ArrayLength for UTerm {
    type HalfSize = U0;
}

impl<N: ArrayLength> ArrayLength for UInt<N, B0> {
    type HalfSize = N;
}

impl<N: ArrayLength> ArrayLength for UInt<N, B1> {
    type HalfSize = N;
}
